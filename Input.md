# Input


*This "input" component have to put inside "form" component.

Sample API => https://school.edudios.com/sis/api2/home/2156E15A568C2C7992E7E0EED2FBD82B&token=28EF22F5F8ABDCC63E73F9EC6E5ACD00&app_ref_id=5F01CFD3078E7D5758AD0F54583F56FD

props                     | type                            | description           | default
:------------------------ |:-------------------------------------- | --------------:|:------------------
type                    |                            |         "input" | 
label            | String        |         Label to display for this input. Example: "Parent IC" | null
default               | String   |         default value. Example: "9283910742282" | null
placeholder               | String   |          | null
isValueChange               | String   |         this key is to let app to distinguish all the input. Example: "IsIcChange"  | null
value               | String   |         name for this input and also key to get for API. Example: "ic" | null
secure               | Boolean   |         for password field  | false


## Example Usage

```json

[
    {
        "data":[
        {
            type: "form",
            title: "You're able to edit and update your personal profile here.",
            data: [
            
            // -----Start from here-----//
            
            {
                type: "input",
                label: "Phone Number",
                default: "",
                placeholder: "0128381928",
                isValueChange: "isPhoneChange",
                value: "phone",
                secure: false,
            },   
            
            // ----- End -----//
        ]
    }
]
```