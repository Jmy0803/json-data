

# Calendar


Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
type          | calendar                            |         String |
display       |            |         Array |
css           |        |         Object  |
configuration |      |         Object  |
data          |      |         Object  |

## CSS Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
primaryText        |                      |         Object |
dayText            |  |         Object | null
dayFontSize           |                                                                     |         Number | null
dateFontSize                 |                                              |         Number | 
primaryFontSize                 |                                              |         Number | 
secondFontSize                 |                                              |         Number | 


## Configuration Object
Name                     | Description                            | Type           | Default
:------------------------ |:-------------------------------------- | --------------:|:------------------
showDetails        |  true = while user click that specific date, will show that day details in a card below the calendar  ,mostly will set as true              |         Boolean  |
pastScrollRange   |                         |         Number |
futureScrollRange    |                                                 |         Number |
current   |   default Month when reload  |         String |


![calendar](image/Calendar.png )

---

## Example Usage

```json

{
                 "type":"calendar",
                 "css":{
                     "primaryText":{
                         "fontWeight":"600",
                         "color":"#ffcccc"
                    },
                     "dayText":{
                         "fontWeight":"600",
                         "color":"grey",
                         "textAlign":"center"
                     },
                     "dayFontSize":14,
                     "dateFontSize":22,
                     "primaryFontSize":16,
                     "secondFontSize":12,
                     "secondaryText":{
                    }
                },
                "configuration":{
                    "showDetails":true,
                    "pastScrollRange":11,
                    "futureScrollRange":0,
                    "currentMonth":"2020-02-26"
                },
                "data":{
                    "2020-01-22": {"startingDay": true, "color": "#d3f5f7"},
                    "2020-01-23": {"selected": true, "endingDay": true, "color": "#d3f5f7", "textColor": "black"},
                    "2020-01-01": { "color": "#d3f5f7"},
                    "2020-01-02": { "color": "#d3f5f7"},
                    "2020-01-03": {"selected": true,  "color": "#d3f5f7", "textColor": "black"},
                    "2020-01-06": {"startingDay": true, "color": "#d3f5f7"},
                    "2020-01-07": { "color": "#d3f5f7"},
                    "2020-01-08": {"selected": true, "endingDay": true, "color": "#d3f5f7", "textColor": "black"},
                    "2020-01-09": {"selected": true, "startingDay": true, "endingDay": true, "color": "#ffcccc", "textColor": "black"},
                    "2020-01-17": {"selected": true, "startingDay": true, "endingDay": true, "color": "#ffcccc", "textColor": "black"},
                    "2020-01-10": { "startingDay": true, "endingDay": true, "color": "#d3f5f7",},
                    "2020-01-27": { "startingDay": true, "endingDay": true, "color": "#d3f5f7",},
                    "2020-01-28": { "startingDay": true, "endingDay": true, "color": "#d3f5f7",},
                    "2020-01-29": { "startingDay": true, "endingDay": true, "color": "#d3f5f7",},
                    "2020-01-30": { "startingDay": true, "endingDay": true, "color": "#d3f5f7",},
                    "2020-01-31": { "startingDay": true, "endingDay": true, "color": "#d3f5f7",},
                },
                "display":[
                    {
                        "dateString":"2020-01-09",
                        "date":"09",
                        "day" :"Thusday",
                        "label1":"Absent",
                        "label2":"Sick"
                    },
                    {
                        "dateString":"2020-01-10",
                        "date":"10",
                        "day" :"Friday",
                        "label1":"Present",
                        "label2":"On Time"
                    },
                    {
                        "dateString":"2020-01-17",
                        "date":"17",
                        "day": "Friday",
                        "label1":"Absent",
                        "label2":"Family Issue"
                    },
                ]
            }
            
```



---

